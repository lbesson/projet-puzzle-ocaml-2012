# Makefile du projet Puzzle

all: cleanall puzzle puzzle.opt doc clean

puzzle:
	ocamlc -w -a -o puzzle graphics.cma temps.ml resolution.ml interface.ml interface_utilisateur.ml
	ocamlc -w -a -o create_instance graphics.cma temps.ml resolution.ml interface.ml create_instance.ml
	ocamlc -w -a -o create_instance_sol graphics.cma temps.ml resolution.ml interface.ml create_instance_sol.ml

clean:
	-rm -fv *.cm* *~ *.html *.o .*int .*value .*sed*

cleandoc:
	-rm -rfv Doc/ documentation.* ocamldoc.sty

cleanall: clean cleandoc
	-rm -fv puzzle puzzle.opt create_instance.opt create_instance create_instance_sol.opt create_instance_sol

puzzle.opt:
	ocamlopt -w -a -o puzzle.opt graphics.cmxa temps.ml resolution.ml interface.ml interface_utilisateur.ml
	ocamlopt -w -a -o create_instance.opt graphics.cmxa temps.ml resolution.ml interface.ml create_instance.ml
	ocamlopt -w -a -o create_instance_sol.opt graphics.cmxa temps.ml resolution.ml interface.ml create_instance_sol.ml

index.html: README.markdown
	python -m markdown -v -e UTF-8 README.markdown > index.html

doc:	puzzle puzzle.opt index.html
	-mkdir Doc || echo "Doc/ is already there."
	ocamldoc -hide-warnings -keep-code -colorize-code -html -o documentation -d Doc zenity.mli temps.ml resolution.ml interface.ml create_instance.ml create_instance_sol.ml interface_utilisateur.ml sig_interface.mli sig_resolution.mli

# Lilian BESSON (C) 2012-2014