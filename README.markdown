# Projet Puzzle - par Lilian Besson
#### ENS de Cachan ~ 2012 (L3 Informatique) ~ Projet Programmation 2
#### Auteur: [Lilian Besson](http://besson.qc.to)
#### [Dépôt Git](https://bitbucket.org/lbesson/projet-puzzle-ocaml-2012)
#### [Récupérer le code source](https://bitbucket.org/lbesson/projet-puzzle-ocaml-2012/get/master.zip)
#### Binaire autonome :
Un binaire autonome pour GNU/Linux, sans dépendances et ne nécessitant aucune installation, est disponible en téléchargement :
[Puzzle-x86_64.AppImage](https://bitbucket.org/lbesson/projet-puzzle-ocaml-2012/downloads/Puzzle-x86_64.AppImage) [![Download "Puzzle-x86_64.AppImage" (832.0 Ko)](https://img.shields.io/badge/Size-832.0%20Ko-lightgreen.svg)](https://bitbucket.org/lbesson/projet-puzzle-ocaml-2012/downloads/Puzzle-x86_64.AppImage) ([PGP Signature](https://bitbucket.org/lbesson/projet-puzzle-ocaml-2012/downloads/Puzzle-x86_64.AppImage.asc)), empaqueté grâce à [AppImage](https://AppImage.org/).

#### Licence : GPLv3
(Voir [LICENSE](LICENSE) pour plus de détails.)

## Contenu du dossier
### Sujet du projet
Voici le [sujet](sujet.pdf) du projet, explicant ce que nous devions réaliser.

### Illustrations
Voici quelques démonstrations du projet :

Sur une grille 5x5 avec 16 couleurs :

![Sur une grille 5x5 avec 16 couleurs](./Projet_Puzzle__L3_Informatique__ENS_Cachan__Lilian_Besson__3.png)

Résolution automatique (chronométrée) :

![Résolution automatique (chronométrée)](./Projet_Puzzle__L3_Informatique__ENS_Cachan__Lilian_Besson__4.png)

Exemple du haut d'une grille plus grande (avec message d'information copyrights) :

![Exemple du haut d'une grille plus grande (avec message d'information copyrights)](./Projet_Puzzle__L3_Informatique__ENS_Cachan__Lilian_Besson__1.png)


### Des codes sources
Ce dossier contient les fichiers OCaml source (.ml) suivants :

* ``resolution.ml``
  Il s'agit de la librairie de résolution du puzzle demandée.

* ``interface.ml``
  Il s'agit de la libraire d'interface graphique demandée, réalisée en utilisant Graphics et Zenity (GTK).

* ``interface_utilisateur.ml``
  Il s'agit de l'interface graphique. Lance seulement une procédure du module 'interface.ml'. (non documentée : rien à dire)

* ``create_instance.ml``
  Un tout petit programme pour générer une instance aléatoire, de façon intéractive en utilisant zenity. (non documentée en .mli : rien à dire)

* ``create_instance_sol.ml``
  Un tout petit programme pour générer une instance aléatoire ayant une solution, de façon intéractive en utilisant zenity. (non documentée en .mli : rien à dire)

* ``temps.ml``
  Un tout petit module pour chronométrer le temps mis pour les résolutions. Une fois une résolution finie dans l'interface graphique, un message informatif précise le temps mis par OCaml pour résoudre. Attention : n'est pas du tout égal au temps mis pas l'ordinateur à répondre. Mais ça donne un ordre de grandeur de toute manière.


### Des interfaces utilisateurs
Ce dossier contient les fichiers ocaml signature (.mli) suivants :

* ``sig_resolution.mli``
  Il s'agit de la librairie de résolution du puzzle demandée.

* ``sig_interface.mli``
  Il s'agit de l'interface graphique demandée, réalisée en utilisant Graphics et Zenity (GTK).


### De la documentation
[Ce dossier](Doc/) contient les fichiers de documentations obtenus avec OCamlDoc (page principale : [Doc/documentation.html](Doc/documentation.html)).

### Un Makefile
Ce dossier contient un fichier Makefile pour compiler le projet sur une architecture Linux classique, avec ``ocamlc`` et ``ocamlopt``.

#### **Note :** l'interface graphique est réalisée avec Graphics, et Zenity, il est donc nécessaire de les avoir installés.

## Pour compiler à la main
### Ce qui est requis

* Il faut une version complète d'OCaml (3.12.0 ou +) installée, pour compiler soi-même et pour exécuter le code produit ("projet").
* Note : la dernière version stable d'[OCaml 4.03.0](http://caml.inria.fr/pub/distrib/ocaml-4.03/) fonctionne aussi pour ce projet.

* Les outils libres et gratuitement disponibles suivants sont aussi nécessaires au bon fonctionnement de l'exécutable :

  - "zenity", bibliothèque GTK, pour les fenêtres intéractives en bash (utilisée en Ocaml via le module Sys),
  - "import", du logiciel ImageMagick, pour les captures d'images.

Normalement, le fichier Makefile présent dans le dossier permet de compiler le projet,
en faisant simplement "make" dans un terminal (**testé seulement sous Linux**).

## Remarques et points particuliers
### Outils utilisés
* [OCamlDoc](http://caml.inria.fr/pub/docs/manual-ocaml/ocamldoc.html) pour la documentation,
* Le [module ``Graphics``](http://caml.inria.fr/pub/docs/manual-ocaml/libref/Graphics.html) pour l'affichage graphique,
* L'outil [``zenity`` (GTK)](https://wiki.gnome.org/Projects/Zenity) pour l'interface utilisateur,
* L'outil [``import``](https://imagemagick.org/script/import.php) pour les captures d'écrans,
* Bash pour MakeDocumentation.

### Remarques
* Ma procédure de résolution porte le doux nom de "resolution_naive" car elle n'est aucunement optimisée.
  Ainsi, tant qu'aucun progrès n'aura été fait sur ce plan là, il est inutile de lancer la résolution pour une instance avec plus d'une dizaine de pièces ! (J'ai testé jusqu'à 4x4 seulement).
  A noter que plus 'c' est grand, plus la résolution est rapide pour une instance aléatoire. Si vous voulez faire des tests, choisissez plutôt c plus grand que 10.

* Durant la résolution, le seul moyen d'interrompre le travail du programme est de le fermer **brutalement** !

## Copyrights [![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
© 2012-2017 Lilian Besson, sous [licence GPLv3](http://besson.qc.to/LICENSE.html).

[![Maintenance](https://img.shields.io/badge/Maintained%3F-no-red.svg)](https://bitbucket.org/lbesson/projet-puzzle-ocaml-2012/commits)
[![Demandez moi n'importe quoi !](https://img.shields.io/badge/Demandez%20moi-n'%20importe%20quoi-1abc9c.svg)](https://bitbucket.org/lbesson/ama.fr)

[![made-with-python](https://img.shields.io/badge/Made%20with-OCaml-1f425f.svg)](https://ocaml.org/)
[![Analytics](https://ga-beacon.appspot.com/UA-38514290-17/bitbucket.org/lbesson/projet-puzzle-ocaml-2012/README.markdown)](https://bitbucket.org/lbesson/projet-puzzle-ocaml-2012/)

[![ForTheBadge uses-badges](http://ForTheBadge.com/images/badges/uses-badges.svg)](http://ForTheBadge.com)
[![ForTheBadge uses-git](http://ForTheBadge.com/images/badges/uses-git.svg)](https://GitHub.com/)

[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://GitHub.com/Naereen/)
[![ForTheBadge powered-by-electricity](http://ForTheBadge.com/images/badges/powered-by-electricity.svg)](http://ForTheBadge.com)
