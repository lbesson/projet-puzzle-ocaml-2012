(** Une interface fonctionnelle � l'outil Zenity
    Derni�re version ici: http://besson.qc.to/publis/Zenity/
    @author Lilian Besson <lilian.besson[AT]ens-cachan.fr>
*)

exception ZenityOcaml_Erreur_Annule of int
(** L'exception qui est renvoy� si le processus appel� par les
    fonctions suivantes retourne un code d'erreur UNIX sp�cifiant une
    erreur. *)

val ecrire_dans_fichier : chaine:string -> adresse:string -> unit
(** Ecrit la chaine [chaine] dans le fichier contenu � l'adresse
    relative ou absolue [adresse]. L�ve une exception si ce fichier
    est absent. *)

val nom_fichier_temporaire : string
(** Nom du fichier temporaire ou est stock� le r�sultat de l'appel
    � Zenity. *)

val fairezenity : string -> unit
(** Appel zenity avec une commande, et fork le r�sultat dans
    nom_fichier_temporaire*)

(** {6 D�finition des diff�rentes fonctions utiles du module ZenityOcaml } *)
(** Toutes ces fonctions ont des param�tres uniquement optionnels,
    qui sont initialis�s avec les valeurs par d�faut.  Chaque
    fonction a au moins les param�tres communs [~title],
    [~window_icon], [~width], [~height], [~expiration]. *)


(** {7 zenity --calendar} *)

val calendar :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?text:string ->
  ?day:int ->
  ?month:int -> ?year:int -> ?date_format:string -> unit -> string
(** @param title Fixe le titre de la fen�tre GTK+ cr��e par la
    fonction.  @param window_icon Adresse relative ou absolue o�
    aller chercher une image d'icone pour la fen�tre (en b�ta).
    @param width Largeur de la fen�tre. En dehors d'une certaine
    plage de valeurs, n'a plus d'effets.  @param expiration Fixe
    un temps en secondes avant la fermeture automatique de la
    fen�tre. Pas encore support� pour toutes les fonctions (en
    b�ta).  @param text Fixe le message qui s'affiche dans la
    boite de dialogue.  @param day Num�ro du jour
    pr�-s�lectionn� dans la boite de dialogue (entre 1 et 31).
    @param month Idem pour le mois (entre 1 et 12).  @param year
    Idem pour l'ann�e.  @param date_format Not for casual
    users. *)
(** Ouvre une demande de s�lection de date. *)

(** {7 zenity --entry} *)

val entry :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?text:string -> ?entry_text:string -> ?hide_text:bool -> unit -> string
(** @param entry_text Fixe le texte d�j� �crit par d�faut dans la
    boite de dialogue.  @param hide_text Si pr�sent et vrai (=
    [true]) le texte est cach� lors de la frappe. *)
(** Ouvre une demande de texte. *)


(** {7 zenity --password} *)

val password :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int -> ?expiration:int -> ?username:string -> unit -> string
(** @param username Nom de l'utilisateur auquel on demande le mot de
    passe (facultatif). *)
(** Ouvre une demande de mot de passe. *)


type add_forms =
    Add_entry of string
  (** Ajoute une entr�e requ�te de texte. *)
  | Add_password of string
  (** Ajoute une entr�e requ�te de mot de passe. *)
  | Add_calendar of string
  (** Ajoute une entr�e requ�te de date. *)
  | NoAdd
(** N'ajoute rien. *)
(** Type des entr�es suppl�mentaires de la fonction forms
    (formulaire). *)

val print_add_forms : add_forms -> string
(** Une m�thode simple pour afficher des �l�ments de ce type. *)

(** {7 zenity --forms} *)

val forms :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?text:string ->
  ?separator:string ->
  ?forms_date_format:string -> ?add:add_forms list -> unit -> string
(** @param add Liste des �l�ments ajout�s au formulaire. L'ordre
    compte, et la liste peut etre nulle.  @param separator Inutile
    [pas visible]. *)
(** Ouvre un formulaire. Voir [add_forms] pour plus de d�tails. *)


(** {7 zenity --color-selection} *)

val color_selection :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?color:Graphics.color -> unit -> Graphics.color
(** @param color Fixe la couleur choisie par d�faut pour la fen�tre
    de demande de s�lection de couleur. *)
(** Ouvre une demande de s�lection de couleur. N�cessite le module
    Graphics, inclus dans les versions compil�es zenity.cma et
    zenity.cmxa. Il n'y a normalement rien d'autre � faire pour
    l'utilisation de ZenityOcaml. *)

(** {7 zenity --scale} *)

val scale :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?text:string ->
  ?value:int ->
  ?min_value:int ->
  ?max_value:int ->
  ?step:int -> ?print_partial:bool -> ?hide_value:bool -> unit -> int
(** @param value Fixe la valeur choisie par d�faut pour la fen�tre
    de demande de choix de valeur.  @param min_value Fixe la
    valeur minimale qu'il sera possible de choisir.  @param
    max_value Fixe la valeur maximale qu'il sera possible de
    choisir.  @param step Fixe le pas de l'interval dans lequel il
    sera possible de choisir.  @param hide_value Si pr�sent et
    vrai (= [true]) la valeur courante est cach�e lors de la
    frappe.  @param print_partial Undocumented. *)
(** Ouvre une demande de valeurs enti�res. *)


(** {7 zenity --file-selection} *)

val file_selection :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?filename:string ->
  ?multiple:bool ->
  ?directory:bool ->
  ?save:bool ->
  ?separator:string ->
  ?confirm_overwrite:bool -> ?file_filter:string -> unit -> string list
(** @param filename Nom du fichier surlequel la s�lection est faite
    par d�faut.  @param multiple Si pr�sent et vrai (= [true])
    la s�lection autorise plusieurs fichiers.  @param directory
    Si pr�sent et vrai (= [true]) la s�lection n'autorise que la
    s�lection des dossiers.  @param file_filter Expression
    r�guli�re bash faisant office de filtre sur les fichiers
    s�lectionnables. *)
(** Ouvre une demande de s�lection de fichier dans un explorateur de dossiers. *)


(** {7 zenity --list} *)

val list :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?text:string ->
  ?column:string ->
  ?checklist:bool ->
  ?radiolist:bool ->
  ?separator:string ->
  ?multiple:bool ->
  ?editable:bool ->
  ?print_column:int ->
  ?hide_column:int ->
  ?hide_header:bool -> ?liste:string list -> unit -> string list
(** @param column Item inscrit dans la colonne.  @param liste Liste,
    vide ou non, des �l�ments dans la colonne.  @param multiple Si
    pr�sent et vrai (= [true]) la s�lection autorise plusieurs
    choix.  @param checklist Si pr�sent et vrai (= [true]) la
    s�lection autorise plusieurs choix en cochant. *)
(** Ouvre une demande de s�lection dans une liste construite par
    l'utilisateur (argument liste). Le param�tre separator est
    inutile. Les param�tres non document�s ne sont pas encore bien
    support�s (b�ta). *) (* TODO : fix it *)


(** {7 zenity --text-info} *)

val text_info :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?filename:string ->
  ?editable:bool ->
  ?display:int ->
  ?html:bool ->
  ?url:string -> ?checkbox:string -> ?font:string -> unit -> string
(** @param display Pr�cise le serveur X a utiliser pour afficher la
    fen�tre GTK.  @param editable Permet de rendre le contenu de
    la fen�tre �ditable.  @param checkbox Fixe le message de la
    boite de confirmation.  @param filename Renseigne l'adresse
    relative ou absolue du fichier a afficher.  @param font
    Renseigne la police � utiliser pour afficher le contenu de la
    boite. Une liste compl�te des polices support�es devrait
    voir le jour bient�t.  @param html Doit etre actif pour
    utiliser url, qui affiche le contenu d'une page web (ou d'un
    document en renseignant son adresse URI).  @param url
    Renseigne l'adresse de la page web a afficher.  *)
(** Ouvre une fen�tre informative contenant du texte. *)


(** {7 Edit} *)
  
val edit :
  string ->
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?display:int -> ?font:string -> unit -> string
(** Fournit un �diteur de texte [minimal].  Le premier param�tre
    sans nom correspond � l'adresse relative ou absolue du fichier �
    �diter. Bien sur, les fonctionnalit�s d'un tel �diteur sont
    tr�s limit�es. *)

(** {7 zenity --error} *)

val error :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?text:string -> ?no_wrap:bool -> unit -> unit
(** Ouvre une fen�tre d'erreur. L�ve une exception si l'utilisateur
    n'appuie pas sur le bouton 'positif'. *)


(** {7 zenity --info} *)

val info :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?text:string -> ?no_wrap:bool -> unit -> unit
(** Ouvre une fen�tre informative. L�ve une exception si
    l'utilisateur n'appuie pas sur le bouton 'positif'. *)


(** {7 zenity --question} *)

val question :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?text:string ->
  ?ok_label:string -> ?cancel_label:string -> ?no_wrap:bool -> unit -> unit
(** @param ok_label Fixe le contenu du message de la boite 'positive'
    de confirmation.  @param cancel_label Fixe le contenu du
    message de la boite 'n�gative' d'annulation.  *)
(** Ouvre une demande de r�ponse � une question. L�ve une exception
    si l'utilisateur n'appuie pas sur le bouton 'positif'. *)


(** {7 zenity --warning} *)

val warning :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?text:string -> ?no_wrap:bool -> unit -> unit
(** Ouvre une fen�tre d'avertissement. L�ve une exception si
    l'utilisateur n'appuie pas sur le bouton 'positif'.  *)


(** {7 zenity --notification} *)

val notification :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?text:string -> ?listen:bool -> unit -> int
(** @param listen Not for casual users. *)
(** Ouvre une fen�tre d'avertissement. Ne fait rien si text est
    vide. *)


type urgency_level = Low | Normal | Critical
(** R�pr�sente les trois niveaux d'urgence utilis�es pour la
    fonction notify. *)
val print_urgency_level : urgency_level -> string

(** {7 notify} *)

val notify :
  ?urgency:urgency_level ->
  ?expiration:int ->
  ?icon:string -> ?body:string -> ?summary:string -> unit -> unit
(** @param urgency Niveau d'urgence de la notification.  @param body
    Contenu du message de la notification.  @param summary Titre
    de la notification. *)
(** Affiche une notification syst�me, dans la zone pr�vue � cet
    effet. summary doit etre non vide. *)
  
  
val help : string -> string
(** Affiche l'aide de zenity concernant le topic argument. *)

