(**	Projet Puzzle
	@author Lilian Besson <lilian.besson[AT]ens-cachan.fr> *)

open List;;
open String;;
open Graphics;;

(** Ce script se base sur 'resolution.ml' et 'interface.ml'. Il faut noter qu'un changement dans la librairie de resolution devrait ne pas rendre cette interface obsolète, du moment que les types et les signatures des fonctions principales soient conservées. *)

open Resolution;;
open Interface;;

(*----------------------------------------------------------------------------------------------------------------------------------------------*)

(** Pour sauvegarder l'instance aléatoire générée. *)
let ecrit_instance_generee_sol mon_instance () = 
	Resolution.ecriture_instance ~action_confirmation:Interface.demande_confirmation_graphique ~file:(Interface.ask_value "Donnez un nom de fichier ou ecrire l'instance aléatoire générée (au format décrit en annexe):" ()) ~i:mon_instance;;

(** Fonction launch, utilise zenity. Demande à l'utilisateur des valeurs pour c, n et m, et un nom de fichier destination, et génère une instance aléatoire et l'enregistre. *)
let launch_sol () =
	Interface.zenity_info "Génération d'une instance ayant une solution : lancement !";
	let (m, n, c) = ((Interface.new_m_ic ()), (Interface.new_n_ic ()), (Interface.new_c_ic ())) in
	ecrit_instance_generee_sol (Resolution.generer_instance_solution ~c:c ~n:n ~m:m ()) ();;


print_string "Lancement de l'interface utilisateur de génération d'une instance ayant une solution ...";;
launch_sol ();;

print_string "Interface utilisateur de génération d'une instance ayant une solution du projet Puzzle : terminée.\n Lilian Besson (c) 2012";;